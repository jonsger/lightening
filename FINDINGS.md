# Findings about lightening

## lightening.h

### Glossary
- GPR: general purpose register, for data and addresses
- FPR: floating point register
- `rsh`: ?
- `__GNUC__`: only recognized by gcc
- IMM: immediate (address mode) maybe

## ppc.h

### Registers
POWER9:
- https://wiki.raptorcs.com/wiki/File:POWER9_Registers_vol3_version1.2_pub.pdf
- https://wiki.raptorcs.com/w/images/c/cb/PowerISA_public.v3.0B.pdf

POWER8, POWER9:
general purpose registers, called fixed-point facility registers: 32GPR,
FPR0-31
floating point registers: 32 FPR, FPR0-31

### Required functions

The following functions seemed to be required for `ppc.c` as they are present
in the ported architecures files:
```c

jit_bool_t
jit_get_cpu(void)

jit_bool_t
jit_init(jit_state_t *_jit)

static size_t
jit_initial_frame_size (void)

static void
reset_abi_arg_iterator(struct abi_arg_iterator *iter, size_t argc,
                       const jit_operand_t *args)

static void
next_abi_arg(struct abi_arg_iterator *iter, jit_operand_t *arg)

static void
jit_flush(void *fptr, void *tptr)

static inline size_t
jit_stack_alignment(void)

static void
jit_try_shorten(jit_state_t *_jit, jit_reloc_t reloc, jit_pointer_t addr)

static void*
bless_function_pointer(void *ptr)
```

The following structs, constants, etc:
```c
static const jit_gpr_t abi_gpr_args[]
static const jit_fpr_t abi_fpr_args[]
static const int abi_gpr_arg_count
static const int abi_fpr_arg_count
struct abi_arg_iterator
```
